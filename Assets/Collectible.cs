﻿using UnityEngine;
using System.Collections;

public class Collectible : MonoBehaviour {

	private float rotAngle = 0;
	// Use this for initialization
	void Start () {
		GetComponent<Renderer> ().material.color = generateRandomColor ();
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.Rotate (1, 1, 1);
	}

	Color generateRandomColor()
	{
		int colorCode = Random.Range (1, 3);
		Color colorG = Color.black;
		if (colorCode == 1)
			colorG	= Color.yellow;
		if (colorCode == 2)
			colorG	= Color.magenta;
		return colorG;
	}
}
