﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class Gameover : MonoBehaviour {

	public Button resume;
	public Button quit;
	public Button restart;
	public Text highscore;
	// Use this for initialization
	void Start () {
		quit.onClick.AddListener (quitGame);
		resume.onClick.AddListener (resumeGame);
		restart.onClick.AddListener (restartGame);
		highscore.text = "GameOver!" + '\n' + "Your Score is " + FindObjectOfType<Player> ().highScore.ToString ();
	}

	// Update is called once per frame
	void Update () {

	}

	void quitGame(){
		Application.Quit();
		Debug.Log ("Quit Game");
	}

	void resumeGame(){
		GlobalScript gp = FindObjectOfType<GlobalScript>();
		Player player = FindObjectOfType<Player>();

		player.score = player.highScore;
		player.scoreText.text = "Score: " + player.score.ToString();
		player.isGameOver = false;
		gp.CanPause = true;
		Time.timeScale = 1;

		SceneManager.UnloadScene ("Gameover");

	}

	void restartGame()
	{
		Time.timeScale = 1;
		Application.LoadLevel("Game");
	}
}
