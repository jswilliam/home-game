﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


public class GlobalScript : MonoBehaviour {

	public Transform ground;
	public Transform collectible;
	public bool CanPause;
	public Button pauseButton;
	public GameObject player;


	void Start() {
		CanPause = true;
		for (float z = 5.2f; z < 100; z+=5.5f) {
			Instantiate(ground, new Vector3(-0.7053299f, 0.037243f, z), Quaternion.identity);
		}
		for (float z = 5.2f; z < 100; z+=randomPositionZ()) {
			Instantiate(collectible, new Vector3(randomPositionX(), 1, z), Quaternion.identity);
		}
		pauseButton.onClick.AddListener(pause);

	}

	void generateGround(){
		Ground[] grounds = FindObjectsOfType<Ground>();
		Ground groundLast = grounds[0];
		if (groundLast.transform.position.z - player.transform.position.z < 50) 
		{
			float zNew = groundLast.transform.position.z + 5.5f;
			for (float z = zNew; z < zNew*2; z+=5.5f) {
				Instantiate(ground, new Vector3(-0.7053299f, 0.037243f, z), Quaternion.identity);
			}
		}	

	}

	void Update () {
		generateGround ();
		genrateCollectibles ();
		if (Input.GetKeyDown (KeyCode.Escape)) {
			pause ();
		}
	}

	public bool isPaused()
	{
		return !CanPause;
	}

	void genrateCollectibles()
	{
		Ground[] grounds = FindObjectsOfType<Ground>();
		Ground groundLast = grounds[0];
		Debug.Log (groundLast.transform.position.z - player.transform.position.z);
		if (groundLast.transform.position.z - player.transform.position.z < 50.2f) 
		{
			float zNew = groundLast.transform.position.z + 5.5f;
			for (float z = zNew; z < zNew*2; z+=randomPositionZ()) {
				Instantiate(collectible, new Vector3(randomPositionX(), 1, z), Quaternion.identity);
			}
		}
	}

	float randomPositionX()
	{
		return Random.Range (-4, 2.8f);
	}

	float randomPositionZ()
	{
		return Random.Range (1, 50);
	}

	void pause()
	{
		if(CanPause){
			Debug.Log("pause");
			CanPause = false;
			Time.timeScale = 0;
			SceneManager.LoadScene ("Pause", LoadSceneMode.Additive);
		}
	}

}
