﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {
	
	public Rigidbody rb;
	public Renderer renderer;
	float acc = 3.0f;
	public int score = 100;
	public int highScore = 0;
	public Text scoreText;
	public bool isGameOver;
	public Ground[] grounds;
	public GameObject groundPrefab;
    public float grayTime;
    bool isGray;
	bool jumped;
	WallLight[] walls;

	public AudioClip jumpSound;
	public AudioClip collectibleSound;
	public AudioClip yellowCollectibleSound;
	public AudioClip rollingSound;
	public AudioClip purpleTimerSound;
	public AudioClip groundHitSound;
	public AudioClip gameOverSound;
	public AudioClip failMatch;
	public AudioClip correctMatch;
	private AudioSource source;


	// Use this for initialization
	void Start () {
        isGray = false;
		rb = GetComponent<Rigidbody>();
		renderer = GetComponent<Renderer> ();
		renderer.material.color = Color.blue;
		isGameOver = false;
	}

	void Awake () {

		source = GetComponent<AudioSource>();

	}

	void updateHighScore()
	{
		if (highScore < score)
			highScore = score;
	}
	
	// Update is called once per frame
	void Update ()
	{
        
		if (isGameOver) {
			Time.timeScale = 0;
			source.Stop ();
			source.PlayOneShot (gameOverSound, 1.0f);
			SceneManager.LoadScene ("Gameover", LoadSceneMode.Additive);
			isGameOver = false;
		}

        
		if (!FindObjectOfType<GlobalScript> ().isPaused ()) {

			destroyGround();
			destroyCollectibles ();
			updateHighScore ();
			rb.AddForce(Vector3.forward*acc, ForceMode.Acceleration);

			if (rb.position.y <= 0.7 && jumped) {
				source.PlayOneShot (groundHitSound, 1.0f);
				jumped = false;
			}
			if (Time.time - grayTime > 5 && isGray) {
				restoreGrounds ();
			}

			if (Input.GetKey (KeyCode.RightArrow)) {
				rb.AddForce (Vector3.right * 0.5f, ForceMode.VelocityChange);
			}
			if (Input.GetKey (KeyCode.LeftArrow)) {
				rb.AddForce (Vector3.left * 0.5f, ForceMode.VelocityChange);
			}
			if (Input.GetKeyUp (KeyCode.Space) && rb.position.y < 2) {
				rb.AddForce (Vector3.up * 5f, ForceMode.VelocityChange);
				source.PlayOneShot (jumpSound, 1.0f);
				jumped = true;
			}
			if (Input.GetKey (KeyCode.Q)) {
				renderer.material.color = Color.red;
				changeLightsColor (Color.red);
			}
			if (Input.GetKey (KeyCode.W)) {
				renderer.material.color = Color.blue;
				changeLightsColor (Color.blue);
			}
			if (Input.GetKey (KeyCode.E)) {
				renderer.material.color = Color.green;
				changeLightsColor (Color.green);
			}
		}
	}

	void changeLightsColor(Color color)
	{
		walls = FindObjectsOfType<WallLight> ();
		foreach (WallLight wall in walls)
		{
			wall.GetComponent<Light> ().color = color;
			wall.source.PlayOneShot (wall.changeColorSound, 0.2f);
		}
	}
    void restoreGrounds()
    {
        grounds = FindObjectsOfType<Ground>();
        foreach (Ground ground in grounds)
        {
            ground.GetComponent<Renderer>().material.color = generateRandomColor();
        }
        isGray = false;
		source.Stop();
		source.loop = enabled;
		source.clip = rollingSound;
		source.Play ();
    }

    Color generateRandomColor()
    {
        int colorCode = Random.Range(1, 5);
        Color colorG = Color.black;
        if (colorCode == 1)
            colorG = Color.red;
        if (colorCode == 2)
            colorG = Color.blue;
        if (colorCode == 3)
            colorG = Color.green;
        if (colorCode == 4)
            colorG = Color.gray;

        return colorG;
    }

    void destroyGround()
    {
        WholeGround[] grounds = FindObjectsOfType<WholeGround>();
        foreach (WholeGround ground in grounds)
        {
            if (rb.transform.position.z - ground.transform.position.z > 10)
            {
                Destroy(ground.gameObject);
            }
                
        }
    }

	void destroyCollectibles()
	{
		Collectible[] collectibles = FindObjectsOfType<Collectible>();
		foreach (Collectible collectible in collectibles)
		{
			if (rb.transform.position.z - collectible.transform.position.z > 10)
			{
				Destroy(collectible.gameObject);
			}

		}
	}

    void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("Collectible") && other.gameObject.GetComponent<Renderer> ().material.color == Color.yellow) 
		{
			Destroy (other.gameObject);
			score += 20;
			scoreText.text = "Score: " + score;
			source.PlayOneShot (yellowCollectibleSound, 0.5f);
		}

		if (other.gameObject.CompareTag ("Collectible") && other.gameObject.GetComponent<Renderer> ().material.color == Color.magenta) 
		{
			source.PlayOneShot (collectibleSound, 1.0f);
			Destroy (other.gameObject);
			grounds = FindObjectsOfType<Ground>();
			foreach (Ground ground in grounds) {
				ground.GetComponent<Renderer> ().material.color = Color.gray;
                grayTime = Time.time;
                isGray = true;
			}
			source.PlayOneShot(purpleTimerSound, 1.0f);

		}

		if (other.gameObject.CompareTag ("Ground") &&
		    other.gameObject.GetComponent<Renderer> ().material.color != renderer.material.color) {
			if (other.gameObject.GetComponent<Renderer> ().material.color == Color.gray) {
				source.PlayOneShot (correctMatch, 1.0f);
				return;
			}
			score /= 2;
			source.PlayOneShot (failMatch, 1.0f);
			if (score == 0)
				isGameOver = true;
			scoreText.text = "Score: " + score;
		} else {
			source.PlayOneShot (correctMatch, 1.0f);
		}
	}
}
