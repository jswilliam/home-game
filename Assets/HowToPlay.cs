﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class HowToPlay : MonoBehaviour {

	public Button back;
	// Use this for initialization
	void Start () {
		back.onClick.AddListener (backButton);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void backButton(){
		SceneManager.UnloadScene ("HowToPlay");
	}
}
