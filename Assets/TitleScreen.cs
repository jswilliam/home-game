﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


public class TitleScreen : MonoBehaviour {

	// Use this for initialization
	public Button play;
	public Button mute;
	public Button howPlay;
	public bool isMuted;
	void Start () {
		play.onClick.AddListener (startGame);
		mute.onClick.AddListener (muteSound);
		howPlay.onClick.AddListener (howToPlay);
		isMuted = false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

 	void startGame() {
		Application.LoadLevel("Game");
	}

	void muteSound(){
		if (!isMuted) {
			AudioListener.pause = true;
			isMuted = true;
		} else {
			AudioListener.pause = false;
			isMuted = false;
		}
	}
	void howToPlay(){
		SceneManager.LoadScene ("HowToPlay", LoadSceneMode.Additive);
	}
}
