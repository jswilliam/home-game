﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;


public class PauseScreen : MonoBehaviour {

	public Button resume;
	public Button quit;
	// Use this for initialization
	void Start () {
		quit.onClick.AddListener (quitGame);
		resume.onClick.AddListener (resumeGame);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void quitGame(){
		Application.Quit();
		Debug.Log ("Quit Game");
	}

	void resumeGame(){
		GlobalScript gp = FindObjectOfType<GlobalScript>();
		gp.CanPause = true;
		SceneManager.UnloadScene ("Pause");
		Time.timeScale = 1;
	}
		
}
