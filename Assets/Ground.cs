﻿using UnityEngine;
using System.Collections;

public class Ground : MonoBehaviour {

	void Start () {
		GetComponent<Renderer> ().material.color = generateRandomColor ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	Color generateRandomColor()
	{
		int colorCode = Random.Range (1, 5);
		Color colorG = Color.black;
		if (colorCode == 1)
			colorG	= Color.red;
		if (colorCode == 2)
			colorG	= Color.blue;
		if (colorCode == 3)
			colorG	= Color.green;
		if (colorCode == 4)
			colorG = Color.gray;

		return colorG;
	}

}
